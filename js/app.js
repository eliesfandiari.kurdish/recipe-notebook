let $ = document
let titleInput = $.getElementById('title')
let ingredientInput = $.getElementById('ingredient')
let recipeTextarea = $.getElementById('recipe')
let modal = $.getElementById('modal')
let addBtn = $.querySelector('.btn')
let firstDiv = $.getElementById('first-div')
let main = $.getElementById('main')

function validation() {
    let titleValue = titleInput.value
    let ingredientValue = ingredientInput.value

    let recipeValue = recipeTextarea.value
    if (titleValue === "" || ingredientValue === "" || recipeValue === "") {
        modal.style.display = "inline"
        setTimeout(function () {
            modal.style.display = "none"
        }, 2000)
        return false
    }
    return true

}
function handleAddRecipe() {
    const validationResult = validation()
    if (!validationResult) {
        return true
    }
    newList()

}

let listArray = []
function newList() {
    let newListObj = {
        id: listArray.length + 1,
        title: titleInput.value,
        ingredient: ingredientInput.value,
        recipe: recipeTextarea.value
    }
    listArray.push(newListObj)
    addListGenerator(listArray)

}

function addListGenerator(addList) {
    let newAddDivElem, newAddUlElem, newAddIClose, newAddLi1Elem, newAddInput1Elem, newAddIEdit1,
        newAddLi2Elem, newAddInput2Elem, newAddIEdit2, newAddLi3Elem,
        newAddTextareaElem, newAddIEdit3
    addList.forEach(function (list) {


        newAddDivElem = $.createElement('div')
        newAddDivElem.className = "list d-flex"
        newAddDivElem.setAttribute('id', 'second-div')
        newAddUlElem = $.createElement('ul')

        newAddUlElem.className = "form-list ul-list d-flex"
        newAddIClose = $.createElement('i')

        newAddIClose.className = "fa fa-times close pointer"
        newAddIClose.addEventListener("click", function (event) {
            event.target.parentElement.remove()
        })

        newAddLi1Elem = $.createElement('li')

        newAddLi1Elem.className = "list-item d-flex"
        newAddInput1Elem = $.createElement('input')

        newAddInput1Elem.className = "label"
        newAddInput1Elem.value = list.title
        newAddInput1Elem.setAttribute('type', 'text')
        newAddInput1Elem.setAttribute('readonly', '')
        newAddInput1Elem.setAttribute('id', 'edit1')
        newAddIEdit1 = $.createElement('i')

        newAddIEdit1.className = "fa fa-edit edit pointer"
        newAddLi2Elem = $.createElement('li')

        newAddLi2Elem.className = "list-item d-flex"
        newAddInput2Elem = $.createElement('input')

        newAddInput2Elem.className = "label"
        newAddInput2Elem.value = list.ingredient
        newAddInput2Elem.setAttribute('type', 'text')
        newAddInput2Elem.setAttribute('readonly', '')
        newAddInput2Elem.setAttribute('id', 'edit2')
        newAddIEdit2 = $.createElement('i')

        newAddIEdit2.className = "fa fa-edit edit pointer"
        newAddLi3Elem = $.createElement('li')

        newAddLi3Elem.className = "list-item overflow d-flex"
        newAddTextareaElem = $.createElement('textarea')

        newAddTextareaElem.className = "label"
        newAddTextareaElem.value = list.recipe
        newAddTextareaElem.setAttribute('type', 'text')
        newAddTextareaElem.setAttribute('readonly', '')
        newAddTextareaElem.setAttribute('id', 'edit3')
        newAddIEdit3 = $.createElement('i')

        newAddIEdit3.className = "fa fa-edit edit pointer"
        newAddLi1Elem.append(newAddInput1Elem, newAddIEdit1)


        newAddLi2Elem.append(newAddInput2Elem, newAddIEdit2)

        newAddLi3Elem.append(newAddTextareaElem, newAddIEdit3)

        newAddUlElem.append(newAddLi1Elem, newAddLi2Elem, newAddLi3Elem)

        newAddDivElem.append(newAddUlElem, newAddIClose)

        main.append(newAddDivElem)
        titleInput.value = ''
        ingredientInput.value = ''
        recipeTextarea.value = ''
    })

    addList.splice(0, 1)
}

main.onmouseenter = () => {

    $.querySelectorAll('.fa-edit').forEach((edit) => {
        return (edit.onclick = (e) => {
            let text = e.target.previousElementSibling;
            if (e.target.classList.contains('fa-edit')) {
                e.target.classList.replace("fa-edit", "fa-check")
                text.readOnly = false;
                text.style.background = '#0208284C'
                text.style.color = '#A19E9ECC'
                text.style.boxShadow = "0 0 5px 2px #4DFF6C"
            } else {
                e.target.classList.replace("fa-check", "fa-edit")
                text.readOnly = true;
                text.style.background = 'none'
                text.style.color = '#fff'
                text.style.boxShadow = "none"
            }
        });
    })
}
addBtn.addEventListener("click", handleAddRecipe)

titleInput.addEventListener('keydown', function (event) {
    if (event.code === 'Enter') {
        newList()
    }
})
ingredientInput.addEventListener('keydown', function (event) {
    if (event.code === 'Enter') {
        newList()
    }
})
recipeTextarea.addEventListener('keydown', function (event) {
    if (event.code === 'Enter') {
        newList()
    }
})
