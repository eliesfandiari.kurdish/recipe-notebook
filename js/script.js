// let $ = document
// let titleInput = $.querySelector('#title');
// let ingredientInput = $.querySelector('#ingredient')
// let recipeTextarea = $.querySelector('#recipe')
// let modal = $.querySelector('.modal')
// let addBtn = $.getElementById('add-btn')
//
//
// function dataValidation() {
//     // alert("invalidation")
//
//     let titleValue = titleInput.value
//     let ingredientValue = ingredientInput.value
//     let recipeValue = recipeTextarea.value
//
//     if (titleValue === '' || ingredientValue === '' || recipeValue === '') {
//
//         modal.style.display = 'inline'
//
//         setTimeout(function () {
//             // modal.style.display = "none"
//         }, 4000)
//         return false
//     }
//
//     return true
//
// }
//
// function handleAddRecipe() {
//     const validationResult = dataValidation()
//     if (!validationResult) {
//         console.log('valid')
//         return true
//     }
//     // newList()
//
// }
//
// // let listArray = []
// //
// // function newList() {
// //     let newListObj = {
// //         id: listArray.length + 1,
// //         title: titleInput.value,
// //         ingredient: ingredientInput.value,
// //         recipe: recipeTextarea.value
// //     }
// //     listArray.push(newListObj)
// //     addListGenerator(listArray)
// // }
// //
// // function addListGenerator(addList) {
// //     let newAddDivElem, newAddUlElem, newAddLi1Elem, newAddLabel1Elem, newAddDiv1Elem, newAddButtonClose1, newAddIClose1,
// //         newAddButtonEdit1, newAddIEdit1, newAddLi2Elem, newAddLabel2Elem, newAddDiv2Elem, newAddButtonClose2,
// //         newAddIClose2, newAddButtonEdit2, newAddIEdit2, newAddLi3Elem, newAddLabel3Elem, newAddDiv3Elem,
// //         newAddButtonClose3, newAddIClose3, newAddButtonEdit3, newAddIEdit3
// //
// //     addList.forEach(function (list) {
// //
// //         newAddDivElem = $.createElement('div')
// //         newAddDivElem.className = "list d-none d-flex"
// //
// //         newAddUlElem = $.createElement('ul')
// //         newAddUlElem.className = "form-list d-flex"
// //
// //         newAddLi1Elem = $.createElement('li')
// //         newAddLi1Elem.className = "list-item d-flex"
// //
// //         newAddLabel1Elem = $.createElement('label')
// //         newAddLabel1Elem.className = "label"
// //         newAddLabel1Elem.innerHTML = list.title
// //
// //         newAddDiv1Elem = $.createElement('div')
// //
// //         newAddButtonClose1 = $.createElement('button')
// //         newAddButtonClose1.className ="close pointer"
// //
// //         newAddIClose1 = $.createElement('i')
// //         newAddIClose1.className ="fa fa-times"
// //
// //         newAddButtonEdit1 = $.createElement('button')
// //         newAddButtonEdit1.className ="close pointer"
// //
// //         newAddIEdit1 = $.createElement('i')
// //         newAddIEdit1.className = "fa fa-edit"
// //
// //         newAddLi2Elem = $.createElement('li')
// //         newAddLi2Elem.className = "list-item d-flex"
// //
// //         newAddLabel2Elem = $.createElement('label')
// //         newAddLabel2Elem.className = "label"
// //         newAddLabel2Elem.innerHTML = list.ingredient
// //
// //         newAddDiv2Elem = $.createElement('div')
// //
// //         newAddButtonClose2 = $.createElement('button')
// //         newAddButtonClose2.className ="close pointer"
// //
// //         newAddIClose2 = $.createElement('i')
// //         newAddIClose2.className ="fa fa-times"
// //
// //         newAddButtonEdit2 = $.createElement('button')
// //         newAddButtonEdit2.className ="close pointer"
// //
// //         newAddIEdit2 = $.createElement('i')
// //         newAddIEdit2.className = "fa fa-edit"
// //
// //         newAddLi3Elem = $.createElement('li')
// //         newAddLi3Elem.className = "list-item d-flex"
// //
// //         newAddLabel3Elem = $.createElement('label')
// //         newAddLabel3Elem.className = "label"
// //         newAddLabel3Elem.innerHTML = list.recipe
// //
// //         newAddDiv3Elem = $.createElement('div')
// //
// //         newAddButtonClose3 = $.createElement('button')
// //         newAddButtonClose3.className ="close pointer"
// //
// //         newAddIClose3 = $.createElement('i')
// //         newAddIClose3.className ="fa fa-times"
// //
// //         newAddButtonEdit3 = $.createElement('button')
// //         newAddButtonEdit3.className ="close pointer"
// //
// //         newAddIEdit3 = $.createElement('i')
// //         newAddIEdit3.className = "fa fa-edit"
// //
// //         newAddButtonClose1.append(newAddIClose1)
// //         newAddButtonEdit1.append(newAddIEdit1)
// //
// //         newAddDiv1Elem.append(newAddButtonClose1, newAddButtonEdit1)
// //
// //         newAddLi1Elem.append(newAddDiv1Elem)
// //
// //         newAddButtonClose2.append(newAddIClose2)
// //         newAddButtonEdit2.append(newAddIEdit2)
// //
// //         newAddDiv2Elem.append(newAddButtonClose2, newAddButtonEdit2)
// //
// //         newAddLi2Elem.append(newAddDiv2Elem)
// //
// //         newAddButtonClose3.append(newAddIClose3)
// //         newAddButtonEdit3.append(newAddIEdit3)
// //
// //         newAddDiv3Elem.append(newAddButtonClose3, newAddButtonEdit3)
// //
// //         newAddLi3Elem.append(newAddDiv3Elem)
// //
// //         newAddUlElem.append(newAddLi1Elem, newAddLi2Elem, newAddLi3Elem)
// //
// //         newAddDivElem.append(newAddUlElem)
// //
// //     })
// // }
// addBtn.addEventListener('click', handleAddRecipe)
const firstDiv = $.getElementById('first-div')
const secondDiv = $.getElementById('second-div')
let newAddList = $.createElement('div')
firstDiv.onsubmit = function (e) {
    e.preventDefault();

    newAddList.innerHTML = `
            <ul class="form-list d-flex">
                <li class="list-item d-flex">
                    <input type="text" class="label" id="edit1" readonly value="title">
                    <i class="fa fa-times close pointer"></i>
                    <i class="fa fa-edit edit pointer"></i>
                </li>
                <li class="list-item d-flex">
                    <input type="text" class="label" id="edit2" readonly value="ingredients">
                    <i class="fa fa-times close pointer"></i>
                    <i class="fa fa-edit edit pointer"></i>
                </li>
                <li class="list-item overflow d-flex">
                    <textarea class="label" id="edit3" readonly>recipes</textarea>
                    <i class="fa fa-times close pointer"></i>
                    <i class="fa fa-edit edit pointer"></i>
                </li>
            </ul>
    `;
    secondDiv.appendChild(newAddList)
}
